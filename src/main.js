// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import '~/assets/css/_global.scss'
import '~/assets/css/_fonts.scss'

import DefaultLayout from '~/layouts/Default.vue'

export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
  
  head.script.push({
    src: 'https://cdn.jsdelivr.net/npm/conic-gradient@1.0.0/conic-gradient.min.js',
  })
  head.script.push({
    src: 'https://cdn.jsdelivr.net/npm/prefixfree@1.0.0/prefixfree.min.js',
  })
}
